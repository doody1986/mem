package writeback

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/mem/cache"
	"gitlab.com/akita/util"
)

var _ = Describe("Flusher", func() {
	var (
		mockCtrl             *gomock.Controller
		port                 *MockPort
		f                    *flusher
		state                cacheState
		inflightTransactions []*transaction
		directory            *MockDirectory
		bankBuf              *MockBuffer
		inBuf                *MockBuffer
		portSender           *MockBufferedSender
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		port = NewMockPort(mockCtrl)
		directory = NewMockDirectory(mockCtrl)
		directory.EXPECT().WayAssociativity().Return(2).AnyTimes()
		state = cacheStateRunning
		inflightTransactions = nil
		bankBuf = NewMockBuffer(mockCtrl)
		inBuf = NewMockBuffer(mockCtrl)
		portSender = NewMockBufferedSender(mockCtrl)

		f = &flusher{
			port:                 port,
			portSender:           portSender,
			inBuf:                inBuf,
			state:                &state,
			inflightTransactions: &inflightTransactions,
			directory:            directory,
			bankBufs:             []util.Buffer{bankBuf},
		}
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should do nothing if no request", func() {
		port.EXPECT().Retrieve(gomock.Any()).Return(nil)
		ret := f.Tick(10)
		Expect(ret).To(BeFalse())
	})

	It("should start flushing", func() {
		req := cache.NewFlushReq(8, nil, nil)
		port.EXPECT().Retrieve(gomock.Any()).Return(req)

		ret := f.Tick(10)

		Expect(ret).To(BeTrue())
		Expect(f.processingFlush).To(BeIdenticalTo(req))
		Expect(state).To(Equal(cacheStatePreFlushing))
	})

	It("should do nothing if there is inflight transaction", func() {
		state = cacheStatePreFlushing
		inflightTransactions = append(inflightTransactions,
			&transaction{})
		req := cache.NewFlushReq(8, nil, nil)
		f.processingFlush = req

		ret := f.Tick(10)

		Expect(ret).To(BeFalse())
	})

	It("should move to flush stage if no inflight transaction", func() {
		state = cacheStatePreFlushing
		inflightTransactions = nil
		req := cache.NewFlushReq(8, nil, nil)
		f.processingFlush = req

		sets := []cache.Set{
			{Blocks: []*cache.Block{
				{IsDirty: true, IsValid: true},
				{IsDirty: false, IsValid: true},
			}},
			{Blocks: []*cache.Block{
				{IsDirty: true, IsValid: false},
				{IsDirty: false, IsValid: false},
			}},
		}
		directory.EXPECT().GetSets().Return(sets)

		ret := f.Tick(10)

		Expect(ret).To(BeTrue())
		Expect(state).To(Equal(cacheStateFlushing))
		Expect(f.blockToEvict).To(HaveLen(1))
		Expect(f.evictingCount).To(Equal(1))
	})

	It("should do nothing if no block to evict", func() {
		state = cacheStateFlushing
		req := cache.NewFlushReq(8, nil, nil)
		f.processingFlush = req
		f.evictingCount = 10
		inBuf.EXPECT().Pop().Return(nil)

		f.blockToEvict = []*cache.Block{}

		ret := f.Tick(10)

		Expect(ret).To(BeFalse())
	})

	It("should stall if bank buffer is full", func() {
		state = cacheStateFlushing
		req := cache.NewFlushReq(8, nil, nil)
		f.processingFlush = req
		f.evictingCount = 10
		inBuf.EXPECT().Pop().Return(nil)

		blocks := []*cache.Block{{Tag: 0x0}, {Tag: 0x40}}
		f.blockToEvict = []*cache.Block{blocks[0], blocks[1]}

		bankBuf.EXPECT().CanPush().Return(false)

		ret := f.Tick(10)

		Expect(ret).To(BeFalse())
	})

	It("should send read for eviction to bank", func() {
		state = cacheStateFlushing
		req := cache.NewFlushReq(8, nil, nil)
		f.processingFlush = req
		inBuf.EXPECT().Pop().Return(nil)

		blocks := []*cache.Block{{Tag: 0x0}, {Tag: 0x40}}
		f.blockToEvict = []*cache.Block{blocks[0], blocks[1]}
		f.evictingCount = 2

		bankBuf.EXPECT().CanPush().Return(true)
		bankBuf.EXPECT().Push(gomock.Any()).Do(func(trans *transaction) {
			Expect(trans.bankAction).To(Equal(bankReadForEviction))
		})

		ret := f.Tick(10)

		Expect(ret).To(BeTrue())
		Expect(f.blockToEvict).NotTo(ContainElement(blocks[0]))
		Expect(f.blockToEvict).To(ContainElement(blocks[1]))
	})

	It("should extract from inbuf", func() {
		state = cacheStateFlushing
		req := cache.NewFlushReq(8, nil, nil)
		f.processingFlush = req
		f.blockToEvict = []*cache.Block{}

		inBuf.EXPECT().Pop().Return(&mem.DoneRsp{})

		ret := f.Tick(10)

		Expect(ret).To(BeTrue())
		Expect(f.returnedEviction).To(Equal(1))
	})

	It("should stall is port sender is busy", func() {
		state = cacheStateFlushing
		req := cache.NewFlushReq(8, nil, nil)
		f.processingFlush = req
		f.blockToEvict = []*cache.Block{}
		f.evictingCount = 10
		f.returnedEviction = 10

		inBuf.EXPECT().Pop().Return(nil)

		portSender.EXPECT().CanSend(1).Return(false)

		ret := f.Tick(10)

		Expect(ret).To(BeFalse())
	})

	It("should send response if all the blocks are evicted", func() {
		state = cacheStateFlushing
		req := cache.NewFlushReq(8, nil, nil)
		f.processingFlush = req
		f.blockToEvict = []*cache.Block{}
		f.evictingCount = 10
		f.returnedEviction = 9

		inBuf.EXPECT().Pop().Return(&mem.DoneRsp{})

		portSender.EXPECT().CanSend(1).Return(true)
		portSender.EXPECT().Send(gomock.Any()).
			Do(func(rsp *cache.FlushRsp) {
				Expect(rsp.RspTo).To(Equal(req.ID))
			})

		ret := f.Tick(10)

		Expect(ret).To(BeTrue())
		Expect(f.processingFlush).To(BeNil())
		Expect(f.returnedEviction).To(Equal(0))
		Expect(f.evictingCount).To(Equal(0))
		Expect(state).To(Equal(cacheStateRunning))
	})
})
