package writeback

import (
	gomock "github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
)

var _ = Describe("TopParser", func() {
	var (
		mockCtrl     *gomock.Controller
		parser       *topParser
		port         *MockPort
		buf          *MockBuffer
		transactions []*transaction
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		port = NewMockPort(mockCtrl)
		buf = NewMockBuffer(mockCtrl)
		transactions = nil

		parser = &topParser{
			port:         port,
			dirBuf:       buf,
			transactions: &transactions,
			state:        new(cacheState),
		}
		*parser.state = cacheStateRunning
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should return if no req to parse", func() {
		port.EXPECT().Peek().Return(nil)
		ret := parser.Tick(10)
		Expect(ret).To(BeFalse())
	})

	It("should return if the cache is not in running stage", func() {
		*parser.state = cacheStateFlushing
		ret := parser.Tick(10)
		Expect(ret).To(BeFalse())
	})

	It("should return if the dir buf is full", func() {
		read := mem.NewReadReq(10, nil, nil, 0x100, 64)
		port.EXPECT().Peek().Return(read)
		buf.EXPECT().CanPush().Return(false)

		ret := parser.Tick(10)

		Expect(ret).To(BeFalse())
	})

	It("should parse read from top", func() {
		read := mem.NewReadReq(10, nil, nil, 0x100, 64)

		port.EXPECT().Peek().Return(read)
		buf.EXPECT().CanPush().Return(true)
		buf.EXPECT().Push(gomock.Any()).Do(func(t *transaction) {
			Expect(t.read).To(BeIdenticalTo(read))
		})
		port.EXPECT().Retrieve(akita.VTimeInSec(10)).Return(read)

		parser.Tick(10)

		Expect(transactions).To(HaveLen(1))
	})

	It("should parse write from top", func() {
		read := mem.NewWriteReq(10, nil, nil, 0x100)

		port.EXPECT().Peek().Return(read)
		buf.EXPECT().CanPush().Return(true)
		buf.EXPECT().Push(gomock.Any()).Do(func(t *transaction) {
			Expect(t.write).To(BeIdenticalTo(read))
		})
		port.EXPECT().Retrieve(akita.VTimeInSec(10)).Return(read)

		parser.Tick(10)

		Expect(transactions).To(HaveLen(1))
	})

})
