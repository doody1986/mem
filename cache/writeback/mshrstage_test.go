package writeback

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/mem/cache"
)

var _ = Describe("MSHR Stage", func() {
	var (
		mockCtrl            *gomock.Controller
		ms                  *mshrStage
		inBuf               *MockBuffer
		mshr                *MockMSHR
		topSender           *MockBufferedSender
		inflightTransaction []*transaction
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		inBuf = NewMockBuffer(mockCtrl)
		mshr = NewMockMSHR(mockCtrl)
		topSender = NewMockBufferedSender(mockCtrl)
		inflightTransaction = nil
		ms = &mshrStage{
			inBuf:                inBuf,
			mshr:                 mshr,
			topSender:            topSender,
			log2BlockSize:        6,
			inflightTransactions: &inflightTransaction,
		}
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should do nothing if there is no entry in input buffer", func() {
		inBuf.EXPECT().Pop().Return(nil)
		ret := ms.Tick(10)
		Expect(ret).To(BeFalse())
	})

	It("should stall if topSender is busy", func() {
		read := mem.NewReadReq(6, nil, nil, 0x104, 4)
		mshrEntry := &cache.MSHREntry{
			Requests: []interface{}{read},
			Data: []byte{
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
			},
		}
		inBuf.EXPECT().Pop().Return(mshrEntry)
		topSender.EXPECT().CanSend(1).Return(false)

		ret := ms.Tick(10)

		Expect(ret).To(BeFalse())
		Expect(ms.processingMSHREntry).To(BeIdenticalTo(mshrEntry))
	})

	It("should send data ready to top", func() {
		block := &cache.Block{Tag: 0x100}
		read := mem.NewReadReq(6, nil, nil, 0x104, 4)
		trans := &transaction{read: read}
		inflightTransaction = append(inflightTransaction, trans)
		mshrEntry := &cache.MSHREntry{
			Requests: []interface{}{trans},
			Block:    block,
			Data: []byte{
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
			},
		}
		inBuf.EXPECT().Pop().Return(mshrEntry)
		topSender.EXPECT().CanSend(1).Return(true)
		topSender.EXPECT().Send(gomock.Any()).
			Do(func(dr *mem.DataReadyRsp) {
				Expect(dr.Data).To(Equal([]byte{5, 6, 7, 8}))
			})

		ret := ms.Tick(10)

		Expect(ret).To(BeTrue())
		Expect(ms.processingMSHREntry).To(BeNil())
		Expect(inflightTransaction).NotTo(ContainElement(trans))
	})

	It("should send write done to top", func() {
		block := &cache.Block{Tag: 0x100}
		write := mem.NewWriteReq(6, nil, nil, 0x104)
		write.Data = []byte{9, 9, 9, 9}
		trans := &transaction{write: write}
		inflightTransaction = append(inflightTransaction, trans)
		mshrEntry := &cache.MSHREntry{
			Requests: []interface{}{trans},
			Block:    block,
			Data: []byte{
				1, 2, 3, 4, 9, 9, 9, 9,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
			},
		}
		ms.processingMSHREntry = mshrEntry
		topSender.EXPECT().CanSend(1).Return(true)
		topSender.EXPECT().Send(gomock.Any()).
			Do(func(done *mem.DoneRsp) {
				Expect(done.RespondTo).To(Equal(write.ID))
			})

		ret := ms.Tick(10)

		Expect(ret).To(BeTrue())
		Expect(ms.processingMSHREntry).To(BeNil())
		Expect(inflightTransaction).NotTo(ContainElement(trans))
	})
})
