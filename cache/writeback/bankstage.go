package writeback

import (
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/mem/cache"
	"gitlab.com/akita/util"
	"gitlab.com/akita/util/akitaext"
)

type bankStage struct {
	inBuf           util.Buffer
	log2BlockSize   uint64
	latency         int
	storage         *mem.Storage
	topSender       akitaext.BufferedSender
	topPort         akita.Port
	lowModuleFinder cache.LowModuleFinder
	bottomSender    akitaext.BufferedSender
	bottomPort      akita.Port
	mshrStageBuffer util.Buffer

	inFlightTransactions *[]*transaction

	cycleLeft    int
	currentTrans *transaction
}

func (s *bankStage) Tick(now akita.VTimeInSec) bool {
	if s.currentTrans != nil {
		s.cycleLeft--
		if s.cycleLeft < 0 {
			return s.finalizeTrans(now)
		}
		return true
	}
	return s.pullFromBuf()
}

func (s *bankStage) pullFromBuf() bool {
	trans := s.inBuf.Pop()
	if trans == nil {
		return false
	}
	s.cycleLeft = s.latency
	s.currentTrans = trans.(*transaction)
	return true
}

func (s *bankStage) finalizeTrans(now akita.VTimeInSec) bool {
	switch s.currentTrans.bankAction {
	case bankReadHit:
		return s.finalizeReadHit(now)
	case bankWriteHit:
		return s.finalizeWriteHit(now)
	case bankWriteFetched:
		return s.finalizeBankWriteFetched(now)
	case bankReadForEviction:
		return s.finalizeBankReadForEviction(now)
	default:
		panic("bank action not supported")
	}
}

func (s *bankStage) finalizeReadHit(now akita.VTimeInSec) bool {
	if !s.topSender.CanSend(1) {
		s.cycleLeft = 0
		return false
	}

	read := s.currentTrans.read
	addr := read.Address
	_, offset := GetCacheLineID(addr, s.log2BlockSize)
	block := s.currentTrans.block

	data, err := s.storage.Read(block.CacheAddress+offset, read.MemByteSize)
	if err != nil {
		panic(err)
	}

	s.removeTransaction(s.currentTrans)

	s.currentTrans = nil
	block.ReadCount--

	dataReady := mem.NewDataReadyRsp(now, s.topPort, read.Src(), read.ID)
	dataReady.Data = data
	s.topSender.Send(dataReady)

	trace(now, "read hit done", addr, dataReady.Data)

	return true
}

func (s *bankStage) finalizeWriteHit(now akita.VTimeInSec) bool {
	if !s.topSender.CanSend(1) {
		s.cycleLeft = 0
		return false
	}

	write := s.currentTrans.write
	addr := write.Address
	_, offset := GetCacheLineID(addr, s.log2BlockSize)
	block := s.currentTrans.block

	err := s.storage.Write(block.CacheAddress+offset, write.Data)
	if err != nil {
		panic(err)
	}

	s.removeTransaction(s.currentTrans)

	s.currentTrans = nil
	block.IsValid = true
	block.IsLocked = false
	block.IsDirty = true
	if block.DirtyMask == nil {
		block.DirtyMask = make([]bool, 1<<s.log2BlockSize)
	}
	for i := offset; i < offset+uint64(len(write.Data)); i++ {
		block.DirtyMask[i] = true
	}

	done := mem.NewDoneRsp(now, s.topPort, write.Src(), write.ID)
	s.topSender.Send(done)

	trace(now, "write hit done", addr, write.Data)

	return true
}

func (s *bankStage) finalizeBankWriteFetched(now akita.VTimeInSec) bool {
	if !s.mshrStageBuffer.CanPush() {
		s.cycleLeft = 0
		return false
	}

	mshrEntry := s.currentTrans.mshrEntry
	block := mshrEntry.Block
	s.mshrStageBuffer.Push(mshrEntry)
	s.storage.Write(block.CacheAddress, mshrEntry.Data)
	block.IsLocked = false
	block.IsValid = true

	trace(now, "write fetched", block.Tag, mshrEntry.Data)

	s.currentTrans = nil

	return true
}

func (s *bankStage) removeTransaction(trans *transaction) {
	for i, t := range *s.inFlightTransactions {
		if trans == t {
			*s.inFlightTransactions = append(
				(*s.inFlightTransactions)[:i],
				(*s.inFlightTransactions)[i+1:]...)
			return
		}
	}
	panic("transaction not found")
}

func (s *bankStage) finalizeBankReadForEviction(now akita.VTimeInSec) bool {
	if !s.bottomSender.CanSend(1) {
		s.cycleLeft = 0
		return false
	}

	trans := s.currentTrans
	victim := trans.victim
	dst := s.lowModuleFinder.Find(victim.Tag)
	data, err := s.storage.Read(victim.CacheAddress, 1<<s.log2BlockSize)
	if err != nil {
		panic(err)
	}
	write := mem.NewWriteReq(now, s.bottomPort, dst, victim.Tag)
	write.Data = data
	write.DirtyMask = victim.DirtyMask
	trans.eviction = write
	s.bottomSender.Send(write)

	s.currentTrans = nil

	trace(now, "evict", victim.Tag, data)

	return true
}
