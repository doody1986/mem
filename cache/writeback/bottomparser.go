package writeback

import (
	"log"
	"reflect"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/mem/cache"
	"gitlab.com/akita/util"
	"gitlab.com/akita/util/akitaext"
)

type bottomParser struct {
	port             akita.Port
	mshr             cache.MSHR
	bankBufs         []util.Buffer
	pendingEvictions *[]*transaction
	bottomSender     akitaext.BufferedSender
	bottomPort       akita.Port
	lowModuleFinder  cache.LowModuleFinder
	state            *cacheState
	flusherBuffer    util.Buffer
	wayAssocitivity  int
	log2BlockSize    uint64
}

func (p *bottomParser) Tick(now akita.VTimeInSec) bool {
	req := p.port.Peek()
	if req == nil {
		return false
	}

	switch req := req.(type) {
	case *mem.DataReadyRsp:
		return p.handleDataReady(now, req)
	case *mem.DoneRsp:
		return p.handleDoneRsp(now, req)
	default:
		log.Panicf("cannot handle request of type %s\n", reflect.TypeOf(req))
	}

	return false
}

func (p *bottomParser) handleDataReady(
	now akita.VTimeInSec,
	dr *mem.DataReadyRsp,
) bool {
	mshrEntry := p.findMSHREntry(dr)
	block := mshrEntry.Block
	bankNum := bankID(block, p.wayAssocitivity, len(p.bankBufs))

	if !p.bankBufs[bankNum].CanPush() {
		return false
	}

	trace(now, "data ready", block.Tag, dr.Data)

	p.port.Retrieve(now)

	mshrEntry.DataReady = dr
	p.combineData(mshrEntry)
	trans := &transaction{
		mshrEntry:  mshrEntry,
		bankAction: bankWriteFetched,
	}
	p.bankBufs[bankNum].Push(trans)

	p.mshr.Remove(block.PID, block.Tag)

	return true
}

func (p *bottomParser) findMSHREntry(dr *mem.DataReadyRsp) *cache.MSHREntry {
	entries := p.mshr.AllEntries()
	for _, e := range entries {
		if e.ReadReq != nil && dr.RespondTo == e.ReadReq.ID {
			return e
		}
	}
	panic("mshr entry not found")
}

func (p *bottomParser) combineData(mshrEntry *cache.MSHREntry) {
	mshrEntry.Data = mshrEntry.DataReady.Data
	mshrEntry.Block.DirtyMask = make([]bool, 1<<p.log2BlockSize)
	for _, t := range mshrEntry.Requests {
		trans := t.(*transaction)
		if trans.read != nil {
			continue
		}

		mshrEntry.Block.IsDirty = true
		req := trans.write
		_, offset := GetCacheLineID(req.Address, p.log2BlockSize)
		for i := 0; i < len(req.Data); i++ {
			index := offset + uint64(i)
			mshrEntry.Data[index] = req.Data[i]
			mshrEntry.Block.DirtyMask[index] = true
		}
	}
}

func (p *bottomParser) handleDoneRsp(
	now akita.VTimeInSec,
	done *mem.DoneRsp,
) bool {
	if *p.state == cacheStateFlushing {
		return p.handleFlushReturn(now, done)
	}

	trans := p.findEvictionTrans(done)
	trans.evictionDone = done
	if trans.mshrEntry != nil {
		return p.fetch(now, trans)
	}
	return p.writeBank(now, trans)
}

func (p *bottomParser) handleFlushReturn(
	now akita.VTimeInSec,
	done *mem.DoneRsp,
) bool {
	if !p.flusherBuffer.CanPush() {
		return false
	}
	p.flusherBuffer.Push(done)
	p.port.Retrieve(now)
	return true
}

func (p *bottomParser) findEvictionTrans(done *mem.DoneRsp) *transaction {
	for _, t := range *p.pendingEvictions {
		if t.eviction.ID == done.RespondTo {
			return t
		}
	}
	panic("pending eviction not found")
}

func (p *bottomParser) removeEvictionTrans(done *mem.DoneRsp) {
	for i, t := range *p.pendingEvictions {
		if t.eviction.ID == done.RespondTo {
			*p.pendingEvictions = append((*p.pendingEvictions)[:i],
				(*p.pendingEvictions)[i+1:]...)
			return
		}
	}
	panic("pending eviction not found")
}

func (p *bottomParser) fetch(now akita.VTimeInSec, trans *transaction) bool {
	if !p.bottomSender.CanSend(1) {
		return false
	}

	var addr uint64
	if trans.read != nil {
		addr = trans.read.Address
	} else {
		addr = trans.write.Address
	}
	cachelineID, _ := GetCacheLineID(addr, p.log2BlockSize)
	dst := p.lowModuleFinder.Find(cachelineID)
	read := mem.NewReadReq(now,
		p.bottomPort, dst,
		cachelineID, 1<<p.log2BlockSize)
	trans.mshrEntry.ReadReq = read
	p.bottomSender.Send(read)
	p.port.Retrieve(now)

	p.removeEvictionTrans(trans.evictionDone)

	trace(now, "fetch (bottomParser)", read.Address, nil)

	return true
}

func (p *bottomParser) writeBank(
	now akita.VTimeInSec,
	trans *transaction,
) bool {
	bankNum := bankID(trans.block, p.wayAssocitivity, len(p.bankBufs))
	bankBuf := p.bankBufs[bankNum]
	if !bankBuf.CanPush() {
		return false
	}

	trans.bankAction = bankWriteHit
	bankBuf.Push(trans)
	p.port.Retrieve(now)

	p.removeEvictionTrans(trans.evictionDone)

	return true
}
