package writeback

import "gitlab.com/akita/mem"
import "gitlab.com/akita/mem/cache"

type bankAction int

const (
	bankActionInvalid bankAction = iota
	bankReadHit
	bankWriteHit
	bankReadForEviction
	bankWriteFetched
)

type transaction struct {
	read         *mem.ReadReq
	write        *mem.WriteReq
	block        *cache.Block
	victim       *cache.Block
	evictingAddr uint64
	eviction     *mem.WriteReq
	evictionDone *mem.DoneRsp
	mshrEntry    *cache.MSHREntry
	bankAction   bankAction
}
