package writeback

import (
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/util"
)

type topParser struct {
	port         akita.Port
	dirBuf       util.Buffer
	transactions *[]*transaction
	state        *cacheState
}

func (p *topParser) Tick(now akita.VTimeInSec) bool {
	if *p.state != cacheStateRunning {
		return false
	}

	req := p.port.Peek()
	if req == nil {
		return false
	}

	if !p.dirBuf.CanPush() {
		return false
	}

	trans := &transaction{}
	switch req := req.(type) {
	case *mem.ReadReq:
		trans.read = req
	case *mem.WriteReq:
		trans.write = req
	}
	p.dirBuf.Push(trans)

	*p.transactions = append(*p.transactions, trans)

	p.port.Retrieve(now)

	return true
}
