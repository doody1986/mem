package writeback

import (
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem/cache"
	"gitlab.com/akita/util"
	"gitlab.com/akita/util/akitaext"
)

type flusher struct {
	inBuf                util.Buffer
	port                 akita.Port
	portSender           akitaext.BufferedSender
	processingFlush      *cache.FlushReq
	state                *cacheState
	inflightTransactions *[]*transaction
	directory            cache.Directory
	bankBufs             []util.Buffer

	evictingCount    int
	returnedEviction int
	blockToEvict     []*cache.Block
}

func (f *flusher) Tick(now akita.VTimeInSec) bool {
	if f.processingFlush != nil && *f.state == cacheStatePreFlushing {
		return f.processPreFlushing(now)
	}

	if f.processingFlush != nil && *f.state == cacheStateFlushing {
		madeProgress := false
		madeProgress = f.processFlush(now) || madeProgress
		madeProgress = f.processFlushReturn(now) || madeProgress
		madeProgress = f.finalizeFlushing(now) || madeProgress
		return madeProgress
	}

	return f.extractFromPort(now)
}

func (f *flusher) processPreFlushing(now akita.VTimeInSec) bool {
	if len(*f.inflightTransactions) > 0 {
		return false
	}

	*f.state = cacheStateFlushing

	sets := f.directory.GetSets()
	for _, set := range sets {
		for _, block := range set.Blocks {
			if block.ReadCount > 0 || block.IsLocked {
				panic("all the blocks should be unlocked before flushing")
			}

			if block.IsValid && block.IsDirty {
				f.blockToEvict = append(f.blockToEvict, block)
				f.evictingCount++
			}
		}
	}

	return true
}

func (f *flusher) processFlush(now akita.VTimeInSec) bool {
	if len(f.blockToEvict) == 0 {
		return false
	}

	block := f.blockToEvict[0]

	bankNum := bankID(block, f.directory.WayAssociativity(), len(f.bankBufs))
	bankBuf := f.bankBufs[bankNum]

	if !bankBuf.CanPush() {
		return false
	}

	trans := &transaction{
		victim:     block,
		bankAction: bankReadForEviction,
	}
	bankBuf.Push(trans)

	f.blockToEvict = f.blockToEvict[1:]

	return true
}

func (f *flusher) processFlushReturn(now akita.VTimeInSec) bool {
	item := f.inBuf.Pop()
	if item == nil {
		return false
	}

	f.returnedEviction++
	return true
}

func (f *flusher) extractFromPort(now akita.VTimeInSec) bool {
	item := f.port.Retrieve(now)
	if item == nil {
		return false
	}

	req := item.(*cache.FlushReq)
	f.processingFlush = req
	*f.state = cacheStatePreFlushing

	return true
}

func (f *flusher) finalizeFlushing(now akita.VTimeInSec) bool {
	if f.returnedEviction == f.evictingCount {
		if !f.portSender.CanSend(1) {
			return false
		}

		rsp := cache.NewFlushRsp(now,
			f.port, f.processingFlush.Src(), f.processingFlush.ID)
		f.portSender.Send(rsp)

		f.returnedEviction = 0
		f.evictingCount = 0
		f.processingFlush = nil
		*f.state = cacheStateRunning

		return true
	}
	return false
}
