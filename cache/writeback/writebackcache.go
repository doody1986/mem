package writeback

import (
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/mem/cache"
	"gitlab.com/akita/util"
	"gitlab.com/akita/util/akitaext"
)

type cacheState int

const (
	cacheStateInvalid cacheState = iota
	cacheStateRunning
	cacheStatePreFlushing
	cacheStateFlushing
)

// A Cache in writeback package  is a cache that performs the write-back policy
type Cache struct {
	*akita.TickingComponent

	TopPort     akita.Port
	BottomPort  akita.Port
	ControlPort akita.Port

	dirStageBuffer  util.Buffer
	evictorBuffer   util.Buffer
	bankBuffers     []util.Buffer
	mshrStageBuffer util.Buffer
	flusherBuffer   util.Buffer

	topParser    *topParser
	bottomParser *bottomParser
	dirStage     *directoryStage
	bankStages   []*bankStage
	mshrStage    *mshrStage
	flusher      *flusher

	topSender         akitaext.BufferedSender
	bottomSender      akitaext.BufferedSender
	controlPortSender akitaext.BufferedSender

	state                cacheState
	inFlightTransactions []*transaction
	pendingEvictions     []*transaction

	needTick bool
}

// Handle defines how the cache component handles events
func (c *Cache) Handle(e akita.Event) error {
	c.Lock()

	switch evt := e.(type) {
	case akita.TickEvent:
		c.handleTickEvent(evt)
	}

	c.Unlock()
	return nil
}

func (c *Cache) handleTickEvent(evt akita.TickEvent) error {
	now := evt.Time()
	c.needTick = false

	c.needTick = c.controlPortSender.Tick(now) || c.needTick
	c.needTick = c.topSender.Tick(now) || c.needTick
	c.needTick = c.bottomSender.Tick(now) || c.needTick
	c.needTick = c.mshrStage.Tick(now) || c.needTick
	for _, bs := range c.bankStages {
		c.needTick = bs.Tick(now) || c.needTick
	}
	c.needTick = c.bottomParser.Tick(now) || c.needTick
	c.needTick = c.dirStage.Tick(now) || c.needTick
	c.needTick = c.topParser.Tick(now) || c.needTick
	c.needTick = c.flusher.Tick(now) || c.needTick

	if c.needTick {
		c.TickLater(now)
	}

	return nil
}

// NewWriteBackCache creates a new write-through cache,
// injecting the dependency of the engine, the directory and the storage.
func NewWriteBackCache(
	name string,
	engine akita.Engine,
	directory cache.Directory,
	mshr cache.MSHR,
	lowModuleFinder cache.LowModuleFinder,
	storage *mem.Storage,
) *Cache {
	cache := new(Cache)
	cache.TickingComponent = akita.NewTickingComponent(
		name, engine, 1*akita.GHz, cache)

	defaultLog2BlockSize := uint64(6)

	cache.TopPort = akita.NewLimitNumReqPort(cache, 4)
	cache.BottomPort = akita.NewLimitNumReqPort(cache, 4)
	cache.ControlPort = akita.NewLimitNumReqPort(cache, 4)

	cache.dirStageBuffer = util.NewBuffer(1)
	cache.bankBuffers = make([]util.Buffer, 1)
	cache.bankBuffers[0] = util.NewBuffer(1)
	cache.mshrStageBuffer = util.NewBuffer(1)
	cache.flusherBuffer = util.NewBuffer(1)

	cache.topSender = akitaext.NewBufferedSender(
		cache.TopPort, util.NewBuffer(4))
	cache.bottomSender = akitaext.NewBufferedSender(
		cache.BottomPort, util.NewBuffer(4))
	cache.controlPortSender = akitaext.NewBufferedSender(
		cache.ControlPort, util.NewBuffer(4))

	cache.topParser = &topParser{
		port:         cache.TopPort,
		dirBuf:       cache.dirStageBuffer,
		transactions: &cache.inFlightTransactions,
		state:        &cache.state,
	}

	cache.bottomParser = &bottomParser{
		port:             cache.BottomPort,
		mshr:             mshr,
		bankBufs:         cache.bankBuffers,
		pendingEvictions: &cache.pendingEvictions,
		bottomSender:     cache.bottomSender,
		bottomPort:       cache.BottomPort,
		lowModuleFinder:  lowModuleFinder,
		wayAssocitivity:  directory.WayAssociativity(),
		log2BlockSize:    defaultLog2BlockSize,
		flusherBuffer:    cache.flusherBuffer,
		state:            &cache.state,
	}

	cache.dirStage = &directoryStage{
		inBuf:            cache.dirStageBuffer,
		directory:        directory,
		mshr:             mshr,
		bankBufs:         cache.bankBuffers,
		log2BlockSize:    defaultLog2BlockSize,
		lowModuleFinder:  lowModuleFinder,
		bottomSender:     cache.bottomSender,
		toBottomPort:     cache.BottomPort,
		pendingEvictions: &cache.pendingEvictions,
	}

	cache.bankStages = make([]*bankStage, 1)
	cache.bankStages[0] = &bankStage{
		inBuf:                cache.bankBuffers[0],
		log2BlockSize:        defaultLog2BlockSize,
		latency:              1,
		storage:              storage,
		topSender:            cache.topSender,
		topPort:              cache.TopPort,
		bottomSender:         cache.bottomSender,
		bottomPort:           cache.BottomPort,
		lowModuleFinder:      lowModuleFinder,
		mshrStageBuffer:      cache.mshrStageBuffer,
		inFlightTransactions: &cache.inFlightTransactions,
	}

	cache.mshrStage = &mshrStage{
		inBuf:                cache.mshrStageBuffer,
		mshr:                 mshr,
		topSender:            cache.topSender,
		topPort:              cache.TopPort,
		inflightTransactions: &cache.inFlightTransactions,
		log2BlockSize:        defaultLog2BlockSize,
	}

	cache.flusher = &flusher{
		inBuf:                cache.flusherBuffer,
		port:                 cache.ControlPort,
		portSender:           cache.controlPortSender,
		state:                &cache.state,
		inflightTransactions: &cache.inFlightTransactions,
		bankBufs:             cache.bankBuffers,
		directory:            directory,
	}

	cache.state = cacheStateRunning

	return cache
}
