package writeback

import (
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/mem/cache"
	"gitlab.com/akita/util"
	"gitlab.com/akita/util/akitaext"
)

type mshrStage struct {
	inBuf                util.Buffer
	mshr                 cache.MSHR
	topSender            akitaext.BufferedSender
	topPort              akita.Port
	log2BlockSize        uint64
	processingMSHREntry  *cache.MSHREntry
	inflightTransactions *[]*transaction
}

func (s *mshrStage) Tick(now akita.VTimeInSec) bool {
	if s.processingMSHREntry != nil {
		return s.processOneReq(now)
	}

	item := s.inBuf.Pop()
	if item == nil {
		return false
	}

	s.processingMSHREntry = item.(*cache.MSHREntry)
	return s.processOneReq(now)
}

func (s *mshrStage) processOneReq(now akita.VTimeInSec) bool {
	if !s.topSender.CanSend(1) {
		return false
	}

	mshrEntry := s.processingMSHREntry
	trans := mshrEntry.Requests[0].(*transaction)
	s.removeTransaction(trans)
	if trans.read != nil {
		s.respondRead(now, trans.read, mshrEntry.Data)
	} else {
		s.respondWrite(now, trans.write)
	}
	mshrEntry.Requests = mshrEntry.Requests[1:]
	if len(mshrEntry.Requests) == 0 {
		s.processingMSHREntry = nil
	}

	return true
}

func (s *mshrStage) respondRead(
	now akita.VTimeInSec,
	read *mem.ReadReq,
	data []byte,
) {
	_, offset := GetCacheLineID(read.Address, s.log2BlockSize)
	dataReady := mem.NewDataReadyRsp(now, s.topPort, read.Src(), read.ID)
	dataReady.Data = data[offset : offset+read.MemByteSize]
	s.topSender.Send(dataReady)

	trace(now, "read done", read.Address, dataReady.Data)
}

func (s *mshrStage) respondWrite(
	now akita.VTimeInSec,
	write *mem.WriteReq,
) {
	done := mem.NewDoneRsp(now, s.topPort, write.Src(), write.ID)
	s.topSender.Send(done)

	trace(now, "write done", write.Address, write.Data)
}

func (s *mshrStage) removeTransaction(trans *transaction) {
	for i, t := range *s.inflightTransactions {
		if trans == t {
			*s.inflightTransactions = append(
				(*s.inflightTransactions)[:i],
				(*s.inflightTransactions)[i+1:]...)
			return
		}
	}
	panic("transaction not found")
}
