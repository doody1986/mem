package writeback

import (
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/mem/cache"
	"gitlab.com/akita/util"
	"gitlab.com/akita/util/akitaext"
	"gitlab.com/akita/util/ca"
)

type directoryStage struct {
	inBuf            util.Buffer
	directory        cache.Directory
	bankBufs         []util.Buffer
	mshr             cache.MSHR
	log2BlockSize    uint64
	lowModuleFinder  cache.LowModuleFinder
	bottomSender     akitaext.BufferedSender
	toBottomPort     akita.Port
	pendingEvictions *[]*transaction
}

func (ds *directoryStage) Tick(now akita.VTimeInSec) bool {
	item := ds.inBuf.Peek()
	if item == nil {
		return false
	}

	trans := item.(*transaction)
	if trans.read != nil {
		return ds.doRead(now, trans)
	}
	return ds.doWrite(now, trans)
}

func (ds *directoryStage) doRead(
	now akita.VTimeInSec,
	trans *transaction,
) bool {
	trace(now, "read start", trans.read.Address, nil)
	cachelineID, _ := GetCacheLineID(
		trans.read.Address, ds.log2BlockSize)
	block := ds.directory.Lookup(
		trans.read.PID, cachelineID)
	if block != nil {
		return ds.handleReadHit(now, trans, block)
	}
	return ds.handleReadMiss(now, trans)
}

func (ds *directoryStage) handleReadHit(
	now akita.VTimeInSec,
	trans *transaction,
	block *cache.Block,
) bool {
	if block.IsLocked {
		return false
	}

	return ds.readFromBank(trans, block)
}

func (ds *directoryStage) handleReadMiss(
	now akita.VTimeInSec,
	trans *transaction,
) bool {
	req := trans.read
	cacheLineID, _ := GetCacheLineID(req.Address, ds.log2BlockSize)

	mshrEntry := ds.mshr.Query(req.PID, cacheLineID)

	if mshrEntry != nil {
		trans.mshrEntry = mshrEntry
		mshrEntry.Requests = append(mshrEntry.Requests, trans)
		ds.inBuf.Pop()
		return true
	}

	if ds.mshr.IsFull() {
		return false
	}

	victim := ds.directory.FindVictim(cacheLineID)
	if ds.needEviction(victim) {
		return ds.evict(now, trans, victim)
	}

	return ds.fetch(now, trans, victim)
}

func (ds *directoryStage) doWrite(
	now akita.VTimeInSec,
	trans *transaction,
) bool {
	write := trans.write
	trace(now, "write start", trans.write.Address, write.Data)
	cachelineID, _ := GetCacheLineID(write.Address, ds.log2BlockSize)
	block := ds.directory.Lookup(trans.write.PID, cachelineID)

	if block != nil {
		return ds.doWriteHit(trans, block)
	}
	return ds.doWriteMiss(now, trans)
}

func (ds *directoryStage) doWriteHit(
	trans *transaction,
	block *cache.Block,
) bool {
	if block.IsLocked || block.ReadCount > 0 {
		return false
	}
	return ds.writeToBank(trans, block)
}

func (ds *directoryStage) doWriteMiss(
	now akita.VTimeInSec,
	trans *transaction,
) bool {
	write := trans.write
	cachelineID, _ := GetCacheLineID(write.Address, ds.log2BlockSize)

	mshrEntry := ds.mshr.Query(write.PID, cachelineID)
	if mshrEntry != nil {
		trans.mshrEntry = mshrEntry
		mshrEntry.Requests = append(mshrEntry.Requests, trans)
		ds.inBuf.Pop()
		return true
	}

	if ds.isWritingFullLine(write) {
		return ds.writeFullLineMiss(now, trans)
	}
	return ds.writePartialLineMiss(now, trans)
}

func (ds *directoryStage) writeFullLineMiss(now akita.VTimeInSec, trans *transaction) bool {
	write := trans.write
	cachelineID, _ := GetCacheLineID(write.Address, ds.log2BlockSize)

	victim := ds.directory.FindVictim(cachelineID)
	if victim.IsLocked || victim.ReadCount > 0 {
		return false
	}

	if ds.needEviction(victim) {
		return ds.evict(now, trans, victim)
	}

	return ds.writeToBank(trans, victim)
}

func (ds *directoryStage) writePartialLineMiss(
	now akita.VTimeInSec,
	trans *transaction,
) bool {
	write := trans.write
	cachelineID, _ := GetCacheLineID(write.Address, ds.log2BlockSize)

	if ds.mshr.IsFull() {
		return false
	}

	victim := ds.directory.FindVictim(cachelineID)
	if victim.IsLocked || victim.ReadCount > 0 {
		return false
	}

	if ds.needEviction(victim) {
		return ds.evict(now, trans, victim)
	}

	return ds.fetch(now, trans, victim)
}

func (ds *directoryStage) readFromBank(
	trans *transaction,
	block *cache.Block,
) bool {
	numBanks := len(ds.bankBufs)
	bank := bankID(block, ds.directory.WayAssociativity(), numBanks)
	bankBuf := ds.bankBufs[bank]

	if !bankBuf.CanPush() {
		return false
	}

	ds.directory.Visit(block)
	block.ReadCount++
	trans.block = block
	trans.bankAction = bankReadHit
	ds.inBuf.Pop()
	bankBuf.Push(trans)
	return true
}

func (ds *directoryStage) writeToBank(
	trans *transaction,
	block *cache.Block,
) bool {
	numBanks := len(ds.bankBufs)
	bank := bankID(block, ds.directory.WayAssociativity(), numBanks)
	bankBuf := ds.bankBufs[bank]

	if !bankBuf.CanPush() {
		return false
	}

	addr := trans.write.Address
	cachelineID, _ := GetCacheLineID(
		addr, uint64(ds.log2BlockSize))

	ds.directory.Visit(block)
	block.IsLocked = true
	block.Tag = cachelineID
	block.IsValid = true
	block.PID = trans.write.PID
	trans.block = block
	trans.bankAction = bankWriteHit
	ds.inBuf.Pop()
	bankBuf.Push(trans)
	return true
}

func (ds *directoryStage) evict(
	now akita.VTimeInSec,
	trans *transaction,
	victim *cache.Block,
) bool {
	bankNum := bankID(victim, ds.directory.WayAssociativity(), len(ds.bankBufs))
	bankBuf := ds.bankBufs[bankNum]

	if !bankBuf.CanPush() {
		return false
	}

	var addr uint64
	var pid ca.PID
	if trans.read != nil {
		addr = trans.read.Address
		pid = trans.read.PID
	} else {
		addr = trans.write.Address
		pid = trans.write.PID
	}

	cacheLineID, _ := GetCacheLineID(
		addr, uint64(ds.log2BlockSize))

	if !(trans.write != nil && ds.isWritingFullLine(trans.write)) {
		mshrEntry := ds.mshr.Add(pid, cacheLineID)
		mshrEntry.Block = victim
		mshrEntry.Requests = append(mshrEntry.Requests, trans)
		trans.mshrEntry = mshrEntry
	}

	trans.bankAction = bankReadForEviction
	trans.victim = &cache.Block{
		Tag:          victim.Tag,
		CacheAddress: victim.CacheAddress,
		DirtyMask:    victim.DirtyMask,
	}
	trans.block = victim

	victim.Tag = cacheLineID
	victim.PID = pid
	victim.IsLocked = true
	victim.IsDirty = false

	ds.inBuf.Pop()
	bankBuf.Push(trans)

	trans.evictingAddr = trans.victim.Tag
	*ds.pendingEvictions = append(*ds.pendingEvictions, trans)

	trace(now, "evict (dir)", trans.victim.Tag, nil)

	return true
}

func (ds *directoryStage) fetch(
	now akita.VTimeInSec,
	trans *transaction,
	block *cache.Block,
) bool {

	var addr uint64
	var pid ca.PID
	if trans.read != nil {
		addr = trans.read.Address
		pid = trans.read.PID
	} else {
		addr = trans.write.Address
		pid = trans.write.PID
	}
	cacheLineID, _ := GetCacheLineID(
		addr, uint64(ds.log2BlockSize))

	if ds.isEvicting(cacheLineID) {
		return false
	}

	if !ds.bottomSender.CanSend(1) {
		return false
	}

	mshrEntry := ds.mshr.Add(pid, cacheLineID)
	trans.mshrEntry = mshrEntry
	trans.block = block
	block.IsLocked = true
	block.Tag = cacheLineID
	block.PID = pid
	block.IsValid = true
	ds.directory.Visit(block)

	ds.inBuf.Pop()

	read := mem.NewReadReq(
		now,
		ds.toBottomPort,
		ds.lowModuleFinder.Find(cacheLineID),
		cacheLineID,
		1<<ds.log2BlockSize,
	)
	ds.bottomSender.Send(read)

	trace(now, "fetch (dir)", read.Address, nil)

	mshrEntry.ReadReq = read
	mshrEntry.Requests = append(mshrEntry.Requests, trans)
	mshrEntry.Block = block

	return true
}

func (ds *directoryStage) isEvicting(addr uint64) bool {
	for _, t := range *ds.pendingEvictions {
		if t.evictingAddr == addr {
			return true
		}
	}
	return false
}

func (ds *directoryStage) isWritingFullLine(write *mem.WriteReq) bool {
	return len(write.Data) == (1 << ds.log2BlockSize)
}

func (ds *directoryStage) needEviction(victim *cache.Block) bool {
	return victim.IsValid && victim.IsDirty
}
