package cache

import "gitlab.com/akita/akita"

// FlushReq is the request send to a cache unit to request it to flush all
// the cache lines.
type FlushReq struct {
	*akita.ReqBase
}

// NewFlushReq creates a new flush request
func NewFlushReq(time akita.VTimeInSec, src, dst akita.Port) *FlushReq {
	reqBase := akita.NewReqBase()
	reqBase.SetSendTime(time)
	reqBase.SetSrc(src)
	reqBase.SetDst(dst)
	return &FlushReq{
		ReqBase: reqBase,
	}
}

// FlushRsp is the respond sent from the a cache unit for finishing a cache
// flush
type FlushRsp struct {
	*akita.ReqBase
	RspTo string
}

// NewFlushRsp creates a new flush response
func NewFlushRsp(
	time akita.VTimeInSec,
	src, dst akita.Port,
	rspTo string,
) *FlushRsp {
	reqBase := akita.NewReqBase()
	reqBase.SetSendTime(time)
	reqBase.SetSrc(src)
	reqBase.SetDst(dst)
	return &FlushRsp{
		ReqBase: reqBase,
		RspTo:   rspTo,
	}
}
