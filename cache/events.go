package cache

import (
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
)

// A ReadCompleteEvent marks the completion of a cache read
type ReadCompleteEvent struct {
	*akita.EventBase

	ReadReq *mem.ReadReq
	Block   *Block
}

// NewReadCompleteEvent creates a new ReadCompleteEvent
func NewReadCompleteEvent(
	time akita.VTimeInSec,
	handler akita.Handler,
) *ReadCompleteEvent {
	evtBase := akita.NewEventBase(time, handler)
	event := new(ReadCompleteEvent)
	event.EventBase = evtBase
	return event
}

// ReadFromMSHREvent will respond data ready to the sender with the data
// that has not been written into the cache storage
type ReadFromMSHREvent struct {
	*akita.EventBase

	ReadReq *mem.ReadReq
	Data    []byte
}

// NewReadFromMSHREvent creates a new ReadFromMSHREvent
func NewReadFromMSHREvent(
	time akita.VTimeInSec,
	handler akita.Handler,
) *ReadFromMSHREvent {
	evtBase := akita.NewEventBase(time, handler)
	event := new(ReadFromMSHREvent)
	event.EventBase = evtBase
	return event
}

// A ReadForEvictEvent marks the completion of a cache read. The data read
// is going to be written to the low module
type ReadForEvictEvent struct {
	*akita.EventBase

	DataReady     *mem.DataReadyRsp
	EvictBlock    *Block
	NewBlock      *Block
	ReadBottomReq *mem.ReadReq
}

// NewReadForEvictEvent creates a new ReadForEvictEvent
func NewReadForEvictEvent(
	time akita.VTimeInSec,
	handler akita.Handler,
) *ReadForEvictEvent {
	evtBase := akita.NewEventBase(time, handler)
	event := new(ReadForEvictEvent)
	event.EventBase = evtBase
	return event
}

// A WriteCompleteEvent marks the completion of a write completion
type WriteCompleteEvent struct {
	*akita.EventBase

	WriteReq *mem.WriteReq
	Block    *Block
	Offset   uint64
}

// NewWriteCompleteEvent creates a new WriteCompleteEvent
func NewWriteCompleteEvent(
	time akita.VTimeInSec,
	handler akita.Handler,
) *WriteCompleteEvent {
	evtBase := akita.NewEventBase(time, handler)
	evt := new(WriteCompleteEvent)
	evt.EventBase = evtBase
	return evt
}

// WriteFromBottomCompleteEvent is the event that represents a cache write
// operation triggered by a DataReadyRsp from the bottom
type WriteFromBottomCompleteEvent struct {
	*akita.EventBase

	DataReadyRsp *mem.DataReadyRsp
	ReadReq      *mem.ReadReq
	Block        *Block
}

// NewWriteFromBottomCompleteEvent creates a new WriteCompleteEvent
func NewWriteFromBottomCompleteEvent(
	time akita.VTimeInSec,
	handler akita.Handler,
) *WriteFromBottomCompleteEvent {
	evtBase := akita.NewEventBase(time, handler)
	evt := new(WriteFromBottomCompleteEvent)
	evt.EventBase = evtBase
	return evt
}

// ProcessMSHRReturnEvent is an event to let the cache to process the
// requests associate with an MSHREntry
type ProcessMSHRReturnEvent struct {
	*akita.EventBase
}

// NewProcessMSHRReturnEvent creates a new ProcessMSHRReturnEvent
func NewProcessMSHRReturnEvent(
	time akita.VTimeInSec,
	handler akita.Handler,
) *ProcessMSHRReturnEvent {
	evtBase := akita.NewEventBase(time, handler)
	evt := new(ProcessMSHRReturnEvent)
	evt.EventBase = evtBase
	return evt
}

// SendEvent triggers a cache to check its buffer and send requests out.
type SendEvent struct {
	*akita.EventBase
}

// NewSendEvent creates a new SendEvent
func NewSendEvent(
	time akita.VTimeInSec,
	handler akita.Handler,
) *SendEvent {
	evtBase := akita.NewEventBase(time, handler)
	evt := new(SendEvent)
	evt.EventBase = evtBase
	return evt
}

type SetBankCycleLeftToZeroEvent struct {
	*akita.EventBase
	bankNum int
}

func NewSetBankCycleLeftToZeroEvent(
	time akita.VTimeInSec,
	handler akita.Handler,
	bankNum int,
) *SetBankCycleLeftToZeroEvent {
	event := new(SetBankCycleLeftToZeroEvent)
	event.EventBase = akita.NewEventBase(time, handler)
	event.bankNum = bankNum
	return event
}
