package vm

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/util/ca"
)

var _ = Describe("Address Translator", func() {
	var (
		mockCtrl        *gomock.Controller
		topPort         *MockPort
		bottomPort      *MockPort
		translationPort *MockPort
		lowModuleFinder *MockLowModuleFinder

		translator *AddressTranslator
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		topPort = NewMockPort(mockCtrl)
		bottomPort = NewMockPort(mockCtrl)
		translationPort = NewMockPort(mockCtrl)
		lowModuleFinder = NewMockLowModuleFinder(mockCtrl)

		translator = NewAddressTranslator("translator", nil, 1, lowModuleFinder)
		translator.Log2PageSize = 12
		translator.TopPort = topPort
		translator.BottomPort = bottomPort
		translator.TranslationPort = translationPort
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	Context("translate stage", func() {
		var (
			req *mem.ReadReq
		)

		BeforeEach(func() {
			req = mem.NewReadReq(8, nil, nil, 0x100, 4)
			req.PID = 1
			req.IsLastInWave = true
		})

		It("should do nothing if there is no request", func() {
			topPort.EXPECT().Peek().Return(nil)
			madeProgress := translator.translate(10)
			Expect(madeProgress).To(BeFalse())
		})

		It("should merge with existing translation", func() {
			translation := &translation{
				outgoingReq: NewTranslateReq(6, nil, nil, 1, 0x1000, 1),
			}
			translator.translations = append(translator.translations,
				translation)
			req.Address = 0x1040
			topPort.EXPECT().Peek().Return(req)
			topPort.EXPECT().Retrieve(gomock.Any())

			needTick := translator.translate(10)

			Expect(needTick).To(BeTrue())
			Expect(translation.incomingReqs).To(ContainElement(req))
		})

		It("should stall if cannot send for translation", func() {
			topPort.EXPECT().Peek().Return(req)
			translationPort.EXPECT().
				Send(gomock.Any()).
				Return(&akita.SendError{})

			needTick := translator.translate(10)

			Expect(needTick).To(BeFalse())
		})

		It("should send translation", func() {
			var transReq *TranslationReq
			topPort.EXPECT().Peek().Return(req)
			translationPort.EXPECT().Send(gomock.Any()).
				DoAndReturn(func(req *TranslationReq) *akita.SendError {
					transReq = req
					return nil
				})
			topPort.EXPECT().Retrieve(gomock.Any())

			needTick := translator.translate(10)

			Expect(needTick).To(BeTrue())
			Expect(translator.translations).To(HaveLen(1))
			translation := translator.translations[0]
			Expect(translation.outgoingReq).To(BeIdenticalTo(transReq))
			Expect(translation.incomingReqs).To(ContainElement(req))
		})
	})

	Context("forwarding", func() {
		It("should do nothing if there is no translation return", func() {
			translationPort.EXPECT().Retrieve(gomock.Any()).Return(nil)
			needTick := translator.forward(10)
			Expect(needTick).To(BeFalse())
		})

		It("should forward read request", func() {
			req := mem.NewReadReq(6, nil, nil, 0x10040, 4)
			translateReady := NewTranslateReadyRsp(
				8, nil, nil, "")
			translateReady.Page = &Page{PID: 1, VAddr: 0x10000, PAddr: 0x20000}

			translation := &translation{
				incomingReqs: []mem.AccessReq{req},
				incomingRsp:  translateReady,
			}
			translator.translations = append(
				translator.translations, translation)
			translator.forwardingReqs = translation
			lowModuleFinder.EXPECT().Find(uint64(0x20040))
			bottomPort.EXPECT().Send(gomock.Any()).
				Do(func(read *mem.ReadReq) {
					Expect(read).NotTo(BeIdenticalTo(req))
					Expect(read.PID).To(Equal(ca.PID(0)))
					Expect(read.Address).To(Equal(uint64(0x20040)))
					Expect(read.MemByteSize).To(Equal(uint64(4)))
					Expect(read.Src()).To(BeIdenticalTo(bottomPort))
				}).
				Return(nil)

			needTick := translator.forward(10)

			Expect(needTick).To(BeTrue())
			Expect(translator.forwardingReqs).To(BeNil())
			Expect(translator.translations).NotTo(ContainElement(translation))
			Expect(translator.inflightReqToBottom).To(HaveLen(1))
		})

		It("should stall if send failed", func() {
			req := mem.NewReadReq(6, nil, nil, 0x10040, 4)
			translateReady := NewTranslateReadyRsp(
				8, nil, nil, "")
			translateReady.Page = &Page{PID: 1, VAddr: 0x10000, PAddr: 0x20000}

			translation := &translation{
				incomingReqs: []mem.AccessReq{req},
				incomingRsp:  translateReady,
			}
			translator.translations = append(
				translator.translations, translation)
			translator.forwardingReqs = translation
			lowModuleFinder.EXPECT().Find(uint64(0x20040))
			bottomPort.EXPECT().Send(gomock.Any()).
				Do(func(read *mem.ReadReq) {
					Expect(read).NotTo(BeIdenticalTo(req))
					Expect(read.PID).To(Equal(ca.PID(0)))
					Expect(read.Address).To(Equal(uint64(0x20040)))
					Expect(read.MemByteSize).To(Equal(uint64(4)))
					Expect(read.Src()).To(BeIdenticalTo(bottomPort))
				}).
				Return(&akita.SendError{})

			needTick := translator.forward(10)

			Expect(needTick).To(BeFalse())
			Expect(translator.forwardingReqs).To(BeIdenticalTo(translation))
			Expect(translator.translations).To(ContainElement(translation))
			Expect(translator.inflightReqToBottom).To(HaveLen(0))
		})

		It("should forward write request", func() {
			data := []byte{1, 2, 3, 4}
			write := mem.NewWriteReq(6, nil, nil, 0x10040)
			write.Data = data

			transReq := NewTranslateReq(6, nil, nil, 1, 0x10000, 1)
			translateReady := NewTranslateReadyRsp(
				8, nil, nil, transReq.ID)
			translateReady.Page = &Page{PID: 1, VAddr: 0x10000, PAddr: 0x20000}

			translation := &translation{
				incomingReqs: []mem.AccessReq{write},
				outgoingReq:  transReq,
				incomingRsp:  translateReady,
			}
			translator.translations = append(
				translator.translations, translation)
			lowModuleFinder.EXPECT().Find(uint64(0x20040))
			translationPort.EXPECT().Retrieve(gomock.Any()).
				Return(translateReady)
			bottomPort.EXPECT().Send(gomock.Any()).
				Do(func(req *mem.WriteReq) {
					Expect(req).NotTo(BeIdenticalTo(write))
					Expect(req.PID).To(Equal(ca.PID(0)))
					Expect(req.Address).To(Equal(uint64(0x20040)))
					Expect(req.Src()).To(BeIdenticalTo(bottomPort))
					Expect(req.Data).To(Equal(data))
				}).
				Return(nil)

			needTick := translator.forward(10)

			Expect(needTick).To(BeTrue())
			Expect(translator.forwardingReqs).To(BeNil())
			Expect(translator.translations).NotTo(ContainElement(translation))
			Expect(translator.inflightReqToBottom).To(HaveLen(1))
		})
	})

	Context("respond", func() {
		var (
			readFromTop   *mem.ReadReq
			writeFromTop  *mem.WriteReq
			readToBottom  *mem.ReadReq
			writeToBottom *mem.WriteReq
		)

		BeforeEach(func() {
			readFromTop = mem.NewReadReq(8, nil, nil, 0x10040, 4)
			readToBottom = mem.NewReadReq(9, nil, nil, 0x20040, 4)
			writeFromTop = mem.NewWriteReq(8, nil, nil, 0x10080)
			writeToBottom = mem.NewWriteReq(8, nil, nil, 0x20080)
			translator.inflightReqToBottom = []reqToBottom{
				{reqFromTop: readFromTop, reqToBottom: readToBottom},
				{reqFromTop: writeFromTop, reqToBottom: writeToBottom},
			}

		})

		It("should do nothing if there is no response to process", func() {
			bottomPort.EXPECT().Peek().Return(nil)
			madeProgress := translator.respond(10)
			Expect(madeProgress).To(BeFalse())
		})

		It("should respond data ready", func() {
			dataReady := mem.NewDataReadyRsp(10, nil, nil, readToBottom.ID)
			bottomPort.EXPECT().Peek().Return(dataReady)
			topPort.EXPECT().Send(gomock.Any()).
				Do(func(dr *mem.DataReadyRsp) {
					Expect(dr.RespondTo).To(Equal(readFromTop.ID))
					Expect(dr.Data).To(Equal(dataReady.Data))
				}).
				Return(nil)
			bottomPort.EXPECT().Retrieve(gomock.Any())

			madeProgress := translator.respond(10)

			Expect(madeProgress).To(BeTrue())
			Expect(translator.inflightReqToBottom).To(HaveLen(1))
		})

		It("should respond write done", func() {
			done := mem.NewDoneRsp(10, nil, nil, writeToBottom.ID)
			bottomPort.EXPECT().Peek().Return(done)
			topPort.EXPECT().Send(gomock.Any()).
				Do(func(done *mem.DoneRsp) {
					Expect(done.RespondTo).To(Equal(writeFromTop.ID))
				}).
				Return(nil)
			bottomPort.EXPECT().Retrieve(gomock.Any())

			madeProgress := translator.respond(10)

			Expect(madeProgress).To(BeTrue())
			Expect(translator.inflightReqToBottom).To(HaveLen(1))
		})

		It("should stall if TopPort is busy", func() {
			dataReady := mem.NewDataReadyRsp(10, nil, nil, readToBottom.ID)
			bottomPort.EXPECT().Peek().Return(dataReady)
			topPort.EXPECT().Send(gomock.Any()).
				Do(func(dr *mem.DataReadyRsp) {
					Expect(dr.RespondTo).To(Equal(readFromTop.ID))
					Expect(dr.Data).To(Equal(dataReady.Data))
				}).
				Return(&akita.SendError{})

			madeProgress := translator.respond(10)

			Expect(madeProgress).To(BeFalse())
			Expect(translator.inflightReqToBottom).To(HaveLen(2))
		})
	})
})
