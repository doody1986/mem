package vm

import "gitlab.com/akita/akita"

type translateRespondEvent struct {
	*akita.EventBase
	req *TranslationReq
}

type translateReadyRspEvent struct {
	*akita.EventBase
	req *TranslateReadyRsp
}

type translateRequestEvent struct {
	*akita.EventBase
	req *TranslationReq
}

type TLBFullInvalidateRequestEvent struct {
	*akita.EventBase
	req *TLBFullInvalidateReq
}

type TLBPartialInvalidateRequestEvent struct {
	*akita.EventBase
	req *TLBPartialInvalidateReq
}

type invalidationRespondEvent struct {
	*akita.EventBase
	req *PTEInvalidationReq
}

func newTranslateRespondEvent(
	time akita.VTimeInSec,
	handler akita.Handler,
	req *TranslationReq,
) *translateRespondEvent {
	return &translateRespondEvent{akita.NewEventBase(time, handler), req}
}

func newInvalidationRespondEvent(
	time akita.VTimeInSec,
	handler akita.Handler,
	req *PTEInvalidationReq,
) *invalidationRespondEvent {
	return &invalidationRespondEvent{akita.NewEventBase(time, handler), req}
}

func newTranslateRequestEvent(time akita.VTimeInSec, handler akita.Handler,
	req *TranslationReq,
) *translateRequestEvent {
	return &translateRequestEvent{akita.NewEventBase(time, handler), req}
}

func newTranslateReadyRspEvent(time akita.VTimeInSec, handler akita.Handler,
	req *TranslateReadyRsp,
) *translateReadyRspEvent {
	return &translateReadyRspEvent{akita.NewEventBase(time, handler), req}
}

func newTLBFullInvalidateRequestEvent(time akita.VTimeInSec, handler akita.Handler,
	req *TLBFullInvalidateReq,
) *TLBFullInvalidateRequestEvent {
	return &TLBFullInvalidateRequestEvent{akita.NewEventBase(time, handler), req}

}

func newTLBPartialInvalidateRequestEvent(time akita.VTimeInSec, handler akita.Handler,
	req *TLBPartialInvalidateReq,
) *TLBPartialInvalidateRequestEvent {
	return &TLBPartialInvalidateRequestEvent{akita.NewEventBase(time, handler), req}

}
