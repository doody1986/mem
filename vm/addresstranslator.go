package vm

import (
	"log"
	"reflect"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/mem/cache"
	"gitlab.com/akita/util/akitaext"
	"gitlab.com/akita/util/ca"
)

type translation struct {
	incomingReqs []mem.AccessReq
	outgoingReq  *TranslationReq
	incomingRsp  *TranslateReadyRsp
}

type reqToBottom struct {
	reqFromTop  mem.AccessReq
	reqToBottom mem.AccessReq
}

// AddressTranslator is a component that forwards the read/write requests with
// the address translated from virtual to physical.
type AddressTranslator struct {
	*akitaext.TickingComponent

	TopPort         akita.Port
	BottomPort      akita.Port
	TranslationPort akita.Port

	lowModuleFinder     cache.LowModuleFinder
	TranslationProvider akita.Port
	Log2PageSize        uint64
	GPUID               uint64

	translations        []*translation
	forwardingReqs      *translation
	inflightReqToBottom []reqToBottom
}

// Tick updates state at each cycle
func (t *AddressTranslator) Tick(now akita.VTimeInSec) bool {
	madeProgress := false

	madeProgress = t.forward(now) || madeProgress
	madeProgress = t.translate(now) || madeProgress
	madeProgress = t.respond(now) || madeProgress

	return madeProgress
}

func (t *AddressTranslator) translate(now akita.VTimeInSec) bool {
	item := t.TopPort.Peek()
	if item == nil {
		return false
	}

	req := item.(mem.AccessReq)
	vAddr := req.GetAddress()
	vPageID := t.addrToPageID(vAddr)
	trans := t.findExistingTranslation(req.GetPID(), vPageID)
	if trans != nil {
		trans.incomingReqs = append(trans.incomingReqs, req)
		t.TopPort.Retrieve(now)
		return true
	}

	transReq := NewTranslateReq(now,
		t.TranslationPort, t.TranslationProvider,
		req.GetPID(), vPageID, t.GPUID)
	translation := &translation{
		incomingReqs: []mem.AccessReq{req},
		outgoingReq:  transReq,
	}
	t.translations = append(t.translations, translation)
	err := t.TranslationPort.Send(transReq)
	if err != nil {
		return false
	}

	t.TopPort.Retrieve(now)
	return true
}

func (t *AddressTranslator) forward(now akita.VTimeInSec) bool {
	if t.forwardingReqs == nil {
		rsp := t.TranslationPort.Retrieve(now)
		if rsp == nil {
			return false
		}

		transRsp := rsp.(*TranslateReadyRsp)
		translation := t.findTranslationByReqID(transRsp.RespondTo)
		t.forwardingReqs = translation
	}

	translation := t.forwardingReqs
	reqFromTop := translation.incomingReqs[0]
	translatedReq := t.createTranslatedReq(
		reqFromTop,
		*translation.incomingRsp.Page)
	err := t.BottomPort.Send(translatedReq)
	if err != nil {
		return false
	}

	t.inflightReqToBottom = append(t.inflightReqToBottom,
		reqToBottom{
			reqFromTop:  reqFromTop,
			reqToBottom: translatedReq,
		})
	translation.incomingReqs = translation.incomingReqs[1:]
	if len(translation.incomingReqs) == 0 {
		t.forwardingReqs = nil
		t.removeExistingTranslation(translation)
	}

	return true
}

func (t *AddressTranslator) respond(now akita.VTimeInSec) bool {
	rsp := t.BottomPort.Peek()
	if rsp == nil {
		return false
	}

	var rspToTop mem.MemRsp
	switch rsp := rsp.(type) {
	case *mem.DataReadyRsp:
		reqCombo := t.findReqToBottomByID(rsp.RespondTo)
		reqFromTop := reqCombo.reqFromTop
		drToTop := mem.NewDataReadyRsp(now,
			t.TopPort, reqFromTop.Src(),
			reqFromTop.GetID())
		drToTop.Data = rsp.Data
		rspToTop = drToTop
	case *mem.DoneRsp:
		reqCombo := t.findReqToBottomByID(rsp.RespondTo)
		reqFromTop := reqCombo.reqFromTop
		rspToTop = mem.NewDoneRsp(now,
			t.TopPort, reqFromTop.Src(), reqFromTop.GetID())
	default:
		log.Panicf("cannot handle respond of type %s", reflect.TypeOf(rsp))
	}

	err := t.TopPort.Send(rspToTop)
	if err != nil {
		return false
	}

	t.removeReqToBottomByID(rsp.(mem.MemRsp).GetRespondTo())
	t.BottomPort.Retrieve(now)

	return true
}

func (t *AddressTranslator) createTranslatedReq(
	req mem.AccessReq,
	page Page,
) mem.AccessReq {
	switch req := req.(type) {
	case *mem.ReadReq:
		return t.createTranslatedReadReq(req, page)
	case *mem.WriteReq:
		return t.createTranslatedWriteReq(req, page)
	default:
		log.Panicf("cannot translate request of type %s", reflect.TypeOf(req))
		return nil
	}
}

func (t *AddressTranslator) createTranslatedReadReq(
	req *mem.ReadReq,
	page Page,
) *mem.ReadReq {
	offset := req.Address % (1 << t.Log2PageSize)
	addr := page.PAddr + offset
	clone := mem.NewReadReq(0,
		t.BottomPort,
		t.lowModuleFinder.Find(addr),
		addr,
		req.MemByteSize)
	clone.PID = 0
	return clone
}

func (t *AddressTranslator) createTranslatedWriteReq(
	req *mem.WriteReq,
	page Page,
) *mem.WriteReq {
	offset := req.Address % (1 << t.Log2PageSize)
	addr := page.PAddr + offset
	clone := mem.NewWriteReq(0,
		t.BottomPort,
		t.lowModuleFinder.Find(addr),
		addr)
	clone.Data = req.Data
	clone.PID = 0
	return clone
}

func (t *AddressTranslator) addrToPageID(addr uint64) uint64 {
	return (addr >> t.Log2PageSize) << t.Log2PageSize
}

func (t *AddressTranslator) findExistingTranslation(
	pid ca.PID,
	vPageID uint64,
) *translation {
	for _, t := range t.translations {
		if t.outgoingReq.VAddr == vPageID && t.outgoingReq.PID == pid {
			return t
		}
	}
	return nil
}

func (t *AddressTranslator) findTranslationByReqID(id string) *translation {
	for _, t := range t.translations {
		if t.outgoingReq.ID == id {
			return t
		}
	}
	panic("translation not found")
}

func (t *AddressTranslator) removeExistingTranslation(trans *translation) {
	for i, tr := range t.translations {
		if tr == trans {
			t.translations = append(t.translations[:i], t.translations[i+1:]...)
			return
		}
	}
	panic("translation not found")
}

func (t *AddressTranslator) findReqToBottomByID(id string) reqToBottom {
	for _, r := range t.inflightReqToBottom {
		if r.reqToBottom.GetID() == id {
			return r
		}
	}
	panic("req to bottom not found")
}

func (t *AddressTranslator) removeReqToBottomByID(id string) {
	for i, r := range t.inflightReqToBottom {
		if r.reqToBottom.GetID() == id {
			t.inflightReqToBottom = append(
				t.inflightReqToBottom[:i],
				t.inflightReqToBottom[i+1:]...)
			return
		}
	}
	panic("req to bottom not found")
}

// NewAddressTranslator creates a new address translator
func NewAddressTranslator(
	name string,
	engine akita.Engine,
	freq akita.Freq,
	lowModuleFinder cache.LowModuleFinder,
) *AddressTranslator {
	t := new(AddressTranslator)
	t.TickingComponent = akitaext.NewTickingComponent(name, engine, freq, t)

	t.TopPort = akita.NewLimitNumReqPort(t, 4)
	t.BottomPort = akita.NewLimitNumReqPort(t, 4)
	t.TranslationPort = akita.NewLimitNumReqPort(t, 4)

	t.lowModuleFinder = lowModuleFinder

	return t
}
