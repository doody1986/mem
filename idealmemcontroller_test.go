package mem

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"

	. "github.com/onsi/gomega"
	"gitlab.com/akita/akita"
	"gitlab.com/akita/akita/mock_akita"
)

var _ = Describe("IdealMemController", func() {

	var (
		mockCtrl      *gomock.Controller
		engine        *mock_akita.MockEngine
		memController *IdealMemController
		port          *mock_akita.MockPort
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())

		engine = mock_akita.NewMockEngine(mockCtrl)
		port = mock_akita.NewMockPort(mockCtrl)

		memController = NewIdealMemController("MemCtrl", engine, 1*MB)
		memController.Freq = 1000 * akita.MHz
		memController.Latency = 10
		memController.ToTop = port
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should process read request", func() {
		readReq := NewReadReq(10, nil, memController.ToTop, 0, 4)

		engine.EXPECT().
			Schedule(gomock.AssignableToTypeOf(&readRespondEvent{}))

		memController.Handle(readReq)
	})

	It("should process write request", func() {
		writeReq := NewWriteReq(10, nil, memController.ToTop, 0)
		writeReq.Data = []byte{0, 1, 2, 3}
		writeReq.DirtyMask = []bool{false, false, true, false}
		engine.EXPECT().
			Schedule(gomock.AssignableToTypeOf(&writeRespondEvent{}))

		memController.Handle(writeReq)
	})

	It("should handle read respond event", func() {
		data := []byte{1, 2, 3, 4}
		memController.Storage.Write(0, data)

		readReq := NewReadReq(10, nil, memController.ToTop, 0, 4)
		event := newReadRespondEvent(11, memController, readReq)

		port.EXPECT().Send(gomock.AssignableToTypeOf(&DataReadyRsp{}))

		memController.Handle(event)
	})

	It("should retry read if send DataReady failed", func() {
		data := []byte{1, 2, 3, 4}
		memController.Storage.Write(0, data)
		readReq := NewReadReq(10, nil, memController.ToTop, 0, 4)
		event := newReadRespondEvent(11, memController, readReq)

		port.EXPECT().
			Send(gomock.AssignableToTypeOf(&DataReadyRsp{})).
			Return(&akita.SendError{})

		engine.EXPECT().
			Schedule(gomock.AssignableToTypeOf(&readRespondEvent{}))

		memController.Handle(event)
	})

	It("should handle write respond event without write mask", func() {
		data := []byte{1, 2, 3, 4}
		writeReq := NewWriteReq(10, nil, memController.ToTop, 0)
		writeReq.Data = data
		event := newWriteRespondEvent(11, memController, writeReq)

		port.EXPECT().Send(gomock.AssignableToTypeOf(&DoneRsp{}))

		memController.Handle(event)

		retData, _ := memController.Storage.Read(0, 4)
		Expect(retData).To(Equal([]byte{1, 2, 3, 4}))
	})

	It("should handle write respond event", func() {
		memController.Storage.Write(0, []byte{9, 9, 9, 9})
		data := []byte{1, 2, 3, 4}
		writeReq := NewWriteReq(10, nil, memController.ToTop, 0)
		writeReq.Data = data
		writeReq.DirtyMask = []bool{false, true, false, false}
		event := newWriteRespondEvent(11, memController, writeReq)

		port.EXPECT().Send(gomock.AssignableToTypeOf(&DoneRsp{}))

		memController.Handle(event)
		retData, _ := memController.Storage.Read(0, 4)
		Expect(retData).To(Equal([]byte{9, 2, 9, 9}))
	})

	// Measure("write with write mask", func(b Benchmarker) {
	// 	data := make([]byte, 64)
	// 	writeReq := NewWriteReq(10, nil, memController.ToTop, 0)
	// 	writeReq.Data = data
	// 	writeReq.DirtyMask = []bool{
	// 		true, true, true, true, false, false, false, false,
	// 		true, true, true, true, false, false, false, false,
	// 		true, true, true, true, false, false, false, false,
	// 		true, true, true, true, false, false, false, false,
	// 		true, true, true, true, false, false, false, false,
	// 		true, true, true, true, false, false, false, false,
	// 		true, true, true, true, false, false, false, false,
	// 		true, true, true, true, false, false, false, false,
	// 	}

	// 	event := newWriteRespondEvent(11, memController, writeReq)
	// 	port.EXPECT().Send(gomock.AssignableToTypeOf(&DoneRsp{})).AnyTimes()

	// 	b.Time("write time", func() {
	// 		for i := 0; i < 100000; i++ {
	// 			memController.Handle(event)
	// 		}
	// 	})
	// }, 100)

	It("should retry write respond event, if network busy", func() {
		data := []byte{1, 2, 3, 4}
		writeReq := NewWriteReq(10, nil, memController.ToTop, 0)
		writeReq.Data = data
		event := newWriteRespondEvent(11, memController, writeReq)

		port.EXPECT().
			Send(gomock.AssignableToTypeOf(&DoneRsp{})).
			Return(&akita.SendError{})
		engine.EXPECT().
			Schedule(gomock.AssignableToTypeOf(&writeRespondEvent{}))

		memController.Handle(event)
	})
})
