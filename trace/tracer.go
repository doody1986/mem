package trace

import (
	"io"

	"gitlab.com/akita/akita"
)

// Detail is the detail field of a trace
type Detail struct {
	Address, ByteSize uint64
	Data              []byte
}

// A Tracer is a hook that can record the actions of a memory model into
// traces.
type Tracer struct {
}

// Func defined the behavior of the hook
func (t *Tracer) Func(ctx *akita.HookCtx) {
	// traceInfo, ok := info.(*mem.TraceInfo)

	// if !ok {
	// 	return
	// }

	// detail := new(Detail)
	// detail.Address = traceInfo.Address
	// detail.ByteSize = traceInfo.ByteSize
	// detail.Data = traceInfo.Data

	// if traceInfo.Req != nil {
	// 	t.tracer.Trace(
	// 		traceInfo.Req.GetID(),
	// 		traceInfo.When,
	// 		traceInfo.Where,
	// 		traceInfo.What,
	// 		detail,
	// 	)
	// }
}

// NewTracer creates a new Tracer.
func NewTracer(writer io.Writer) *Tracer {
	t := new(Tracer)
	return t
}
